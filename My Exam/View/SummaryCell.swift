//
//  SummaryCell.swift
//  My Exam
//
//  Created by Sourav Santra on 07/12/19.
//  Copyright © 2019 Arindam. All rights reserved.
//

import UIKit

class SummaryCell: UITableViewCell {

    
    @IBOutlet weak var labelQuestion: UILabel!
    @IBOutlet weak var labelAnswer: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
