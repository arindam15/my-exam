//
//  TestHistoryCell.swift
//  My Exam
//
//  Created by Appsbee Technology on 07/12/19.
//  Copyright © 2019 Arindam. All rights reserved.
//

import UIKit

class TestHistoryCell: UITableViewCell {

    @IBOutlet weak var labelTestTime: UILabel!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var labelFirstQuestion: UILabel!
    @IBOutlet weak var labelFirstQuestionAnswer: UILabel!
    @IBOutlet weak var labelSecondQuestion: UILabel!
    @IBOutlet weak var labelSecondQuestionAnswer: UILabel!
    @IBOutlet weak var labelThirdQuestion: UILabel!
    @IBOutlet weak var labelThirdAnswer: UILabel!
    
    
    var testHistory: Test?
    var indexNumber: Int?
    public func setupValue(){
        if let testHistory = testHistory{
//            labelTestTime.text = "Game \(indexNumber!) \(testHistory.attemptedAt)"
            labelUsername.text = "Username: \(testHistory.savedUser.name)"
            labelFirstQuestion.text = testHistory.savedQuestions.first?.question
            
            guard let firstAnswer = testHistory.savedQuestions.first, let  thirdanswer = testHistory.savedQuestions.last else{
                return
            }
            let secondAnswer = testHistory.savedQuestions[1]
            labelFirstQuestionAnswer.text = "Answer:  \(firstAnswer.selectedAnswers.joined(separator: ","))"
            labelSecondQuestion.text = testHistory.savedQuestions[1].question
            labelSecondQuestionAnswer.text = "Answer:  \(secondAnswer.selectedAnswers.joined(separator: ","))"
            
            labelThirdQuestion.text = testHistory.savedQuestions.last?.question
            labelThirdAnswer.text = "Answer:  \(thirdanswer.selectedAnswers.joined(separator: " "))"

        }
    }
    


}
