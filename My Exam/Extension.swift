//
//  Extension.swift
//  My Exam
//
//  Created by Arindam on 07/12/19.
//  Copyright © 2019 Arindam. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    func  showAlertWithOneAction(title:String, actionTtitle: String, style: UIAlertAction.Style, actionMethod: @escaping () -> Void , message:String){
        DispatchQueue.main.async(execute: {() -> Void in
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: actionTtitle, style: style, handler: { action in
                switch action.style{
                case .default:
                    actionMethod()
                case .cancel:
                    actionMethod()
                case .destructive:
                    actionMethod()
                }
            }))
            
            if let presentedVC = self.presentedViewController, presentedVC is UIAlertController {
                presentedVC.dismiss(animated: true, completion: {
                    self.present(alert, animated: true, completion: nil)
                })
            }
            else {
                self.present(alert, animated: true, completion: nil)
            }
            
        })
    }
}
