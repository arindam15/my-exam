//
//  TestVC.swift
//  My Exam
//
//  Created by Arindam on 07/12/19.
//  Copyright © 2019 Arindam. All rights reserved.
//

import UIKit


class TestVC: UIViewController {
    
    
    let testPageVC = UIPageViewController()
    var test: Test?
    fileprivate var testQuestions: [Question]?
    var count = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.testQuestions = test?.savedQuestions
        
        if (self.testQuestions?.count ?? 0) > 0 {
            let firstTestQuestionVC = self.storyboard?.instantiateViewController(withIdentifier: "TestQuestionVC") as! TestQuestionVC
            firstTestQuestionVC.delegate = self
            
            firstTestQuestionVC.questionModel = self.testQuestions?[self.count]
            self.count += 1
            
            self.testPageVC.setViewControllers([firstTestQuestionVC], direction: .forward, animated: true, completion: nil)
            self.addChild(testPageVC)
            self.view.addSubview(testPageVC.view)
        }
    }
    
    
    fileprivate func presntNextQuestion(isLast: Bool = false) {
        if self.count < (self.testQuestions?.count ?? 0) {
            if let questionModel = self.test?.savedQuestions[self.count]{
            if !questionModel.isMultipleChoseEnable{
                let questionVC = self.storyboard?.instantiateViewController(withIdentifier: "QuestionVC") as! QuestionVC
                questionVC.delegate = self
                questionVC.questionModel = questionModel
                self.testPageVC.setViewControllers([questionVC], direction: .forward, animated: true, completion: { (isCompleted) in
                    self.count += 1

                })
            }
            else{
                let testQuestionVC = self.storyboard?.instantiateViewController(withIdentifier: "TestQuestionVC") as! TestQuestionVC
                testQuestionVC.delegate = self
                testQuestionVC.questionModel = questionModel
                self.testPageVC.setViewControllers([testQuestionVC], direction: .forward, animated: true, completion: { (isCompleted) in
                    self.count += 1
                    testQuestionVC.btnNext.setTitle(isLast ? "Summary" : "Next", for: .normal)
                    
                })
            }
            
            }
            

        }
    }
}


extension TestVC: ButtonNextTappDelegate{
    
    func btnNextTapp() {
        if count != 0 {
            if count == (self.testQuestions?.count ?? 0) - 1 {
                presntNextQuestion(isLast: true)
            } else if count < (self.testQuestions?.count ?? 0) {
                presntNextQuestion()
            } else {
                let summaryVC = self.storyboard?.instantiateViewController(withIdentifier: "TestSummaryVC") as! TestSummaryVC
                summaryVC.currentTest = test
                navigationController?.pushViewController(summaryVC, animated: true)
            }
        }
    }
    
    
}



