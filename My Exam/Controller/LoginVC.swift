//
//  UserNameVC.swift
//  My Exam
//
//  Created by Arindam on 07/12/19.
//  Copyright © 2019 Arindam. All rights reserved.
//

import UIKit
import RealmSwift

class LoginVC: UIViewController, UITextFieldDelegate{
    
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    var testHistory: [Test]?
    var realm: Realm!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        userName.delegate = self
        userName.returnKeyType = .done
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    @IBAction func btnNextTapp(_ sender: UIButton) {
        
        if let name = userName.text, !name.isEmpty, let password = tfPassword.text, !password.isEmpty{
            let realm = try! Realm()
            self.testHistory =  realm.objects(Test.self).map{$0}

            if let testHistory = testHistory, testHistory.count > 0{
                let testHistoryVC = self.storyboard?.instantiateViewController(withIdentifier: "TestHistoryVC") as! TestHistoryVC
                testHistoryVC.testHistory = self.testHistory
                navigationController?.pushViewController(testHistoryVC, animated: true)
            }
            else{
                let user = User(name: name)
                let test = Test(user: user, attemptedAt: getCurrentDate())
                let questions: [Question] = [
                    Question(testId: test.id, question: "Who is the best cricketer in the world?",
                             answers: ["Sachin Tendulkar", "Virat Kohli", "Adam Gilcrisht", "Jacque Kalis"],
                             canSelectMultipleAnswer: false,isMultipleChoseEnable: true),
                    Question(testId: test.id, question: "What are the colors in the Indian natinal flag?.",
                             answers: ["White", "Yellow", "Orange", "Green"],
                             canSelectMultipleAnswer: true, isMultipleChoseEnable: true),
                    Question(testId: test.id, question: "Write something about yourself?.",
                             answers: [],
                             canSelectMultipleAnswer: true, isMultipleChoseEnable: false)
                ]
                
                
                try! realm.write {
                    realm.add(user)
                    realm.add(test)
                    realm.add(questions)
                }
                let testVC = storyboard?.instantiateViewController(withIdentifier: "TestVC") as! TestVC
                testVC.test = test
                navigationController?.pushViewController(testVC, animated: true)
            }
        }
        else{
            
            
        }
    }
    
    
    fileprivate func getCurrentDate()-> String{
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy h:mm a"
        let date = Date()
        return dateFormatter.string(from: date)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
}

