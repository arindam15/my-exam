//
//  QuestionVC.swift
//  My Exam
//
//  Created by Arindam on 07/12/19.
//  Copyright © 2019 Arindam. All rights reserved.
//

import UIKit
import RealmSwift

class QuestionVC: UIViewController {

    @IBOutlet weak var labelQuestion: UILabel!
    @IBOutlet weak var tfAnswer: UITextField!
    
    var selectedAnswes = Set<String>()
    var questionModel: Question?
    var test: Test?
    var realm: Realm!
    var delegate: ButtonNextTappDelegate?

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        realm = try! Realm()
        self.labelQuestion.text = questionModel?.question
        
        
    }
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        if let answer = tfAnswer.text, !answer.isEmpty{
            self.selectedAnswes.removeAll()
            self.selectedAnswes.insert(answer)
            if selectedAnswes.count > 0{
                try! realm.write {
                    questionModel?.selectedAnswers.removeAll()
                    questionModel?.selectedAnswers.append(objectsIn: selectedAnswes)
                }
            }
            delegate?.btnNextTapp()
        }
        
        
    }
    


}
