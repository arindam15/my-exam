//
//  TestSummaryVC.swift
//  My Exam
//
//  Created by Arindam on 07/12/19.
//  Copyright © 2019 Arindam. All rights reserved.
//

import UIKit

class TestSummaryVC: UIViewController {
    
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var tableAnswer: UITableView!
    
    var currentTest: Test?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelUserName.text = "Hello  \(currentTest!.savedUser.name)!!!"
        tableAnswer.dataSource = self
        tableAnswer.delegate = self
    }
    
    @IBAction func buttonFinishTapped(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
        
    }
    
}


extension TestSummaryVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentTest?.savedQuestions.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let summaryCell = tableView.dequeueReusableCell(withIdentifier: "SummaryCell") as! SummaryCell
        if let currentTest = currentTest{
            summaryCell.labelQuestion.text = currentTest.savedQuestions[indexPath.row].question
            summaryCell.labelAnswer.text = currentTest.savedQuestions[indexPath.row].selectedAnswers.joined(separator: ",")
        }
        return summaryCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
}
