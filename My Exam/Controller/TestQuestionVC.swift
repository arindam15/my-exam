//
//  QuestionVC.swift
//  My Exam
//
//  Created by Arindam on 07/12/19.
//  Copyright © 2019 Arindam. All rights reserved.
//

import UIKit
import RealmSwift

enum AnswerSelection {
    case first
    case second
    case third
    case fourth
}

protocol ButtonNextTappDelegate:class {
    func btnNextTapp()
}

class TestQuestionVC: UIViewController {
    
    var selectedAnswes = Set<String>()
    var questionModel: Question?
    var test: Test?
    var realm: Realm!
    var delegate: ButtonNextTappDelegate?
    @IBOutlet weak var labelQuestion: UILabel!
    @IBOutlet weak var labelFirstAnswer: UILabel!
    @IBOutlet weak var buttonFirstAnswer: UIButton!
    
    @IBOutlet weak var labelSecondAnswer: UILabel!
    @IBOutlet weak var buttonSecondANswer: UIButton!
    
    @IBOutlet weak var labelThirdAnswer: UILabel!
    @IBOutlet weak var buttonThirdAnswer: UIButton!
    
    @IBOutlet weak var labelFourthAnswer: UILabel!
    @IBOutlet weak var buttonFourthAnswer: UIButton!
    
    @IBOutlet weak var btnNext: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        realm = try! Realm()
        setupQuestion()
        
        [buttonFirstAnswer, buttonSecondANswer, buttonThirdAnswer, buttonFourthAnswer]
        .setTagIndexWise()
        
        [buttonFirstAnswer, buttonSecondANswer, buttonThirdAnswer, buttonFourthAnswer]
            .setImage(UIImage(named: "icons8-unchecked-checkbox-50"), for: .normal)
        
        [buttonFirstAnswer, buttonSecondANswer, buttonThirdAnswer, buttonFourthAnswer]
            .setImage(UIImage(named: "icons8-checked-checkbox-50"), for: .selected)
    }
    
    fileprivate func setupQuestion(){
        self.labelQuestion.text = self.questionModel?.question
        let answers = self.questionModel!.answers
        self.labelFirstAnswer.text = answers[0]
        self.labelSecondAnswer.text = answers[1]
        self.labelThirdAnswer.text = answers[2]
        self.labelFourthAnswer.text = answers[3]
    }
    
    
    public func chnageButtonText(){
        btnNext.setTitle("Finish", for: .normal)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationItem.hidesBackButton = true
    }
    
    
    
    @IBAction func answerButtonTapped(_ sender: UIButton) {
        guard let answer = questionModel?.answers[sender.tag] else { return}
        if questionModel!.canSelectMultipleAnswer{
            (sender.isSelected) ? (_ = selectedAnswes.remove(answer)) : (_ = selectedAnswes.insert(answer))
            sender.isSelected = !sender.isSelected
        }
        else{
            (sender.isSelected) ? (selectedAnswes.removeAll()) : (selectedAnswes = [answer])
            var allButtons: Set<UIButton> = [buttonFirstAnswer, buttonSecondANswer, buttonThirdAnswer, buttonFourthAnswer]
            allButtons.remove(sender)
            let allOtherButtons = allButtons.map{$0}
            allOtherButtons.setSelected(false)
            sender.isSelected = !sender.isSelected
        }
    }
    
    
    
    
    
    @IBAction func buttonNextTapp(_ sender: UIButton) {
        if selectedAnswes.count > 0{
            try! realm.write {
                questionModel?.selectedAnswers.removeAll()
                questionModel?.selectedAnswers.append(objectsIn: selectedAnswes)
            }
            delegate?.btnNextTapp()
        }
    }
    
    

    
    
    
    
    
}






extension Array where Element == UIButton {
    
    func setImage(_ image: UIImage?, for controlState: UIControl.State) {
        self.forEach { (button) in
            button.setImage(image, for: controlState)
        }
    }
    
    func setSelected(_ isSelected: Bool = true) {
        self.forEach { (button) in
            button.isSelected = isSelected
        }
    }
    
    func setTagIndexWise() {
        for (index, eachButton) in self.enumerated() {
            eachButton.tag = index
        }
    }
    
    
}
