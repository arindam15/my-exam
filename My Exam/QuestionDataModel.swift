//
//  QuestionDataModel.swift
//  My Exam
//
//  Created by Arindam on 07/12/19.
//  Copyright © 2019 Arindam. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift



@objcMembers class User: Object {
    enum Properties: String {
        case id, name
    }
    
    dynamic var id: String = UUID().uuidString
    dynamic var name: String = ""
    
    convenience init(name: String) {
        self.init()
        self.name = name
    }
    
    override class func primaryKey() -> String? {
        return Properties.id.rawValue
    }
    
}


@objcMembers class Test: Object {
    
    enum Properties: String {
        case id, liveUser, savedUser, liveQuestions, savedQuestions, attemptedAt
    }
    
    dynamic var id: String = UUID().uuidString
    var liveUser: User!
    var savedUser: User! {
        get {
            let realm = try! Realm()
            return realm.objects(User.self).filter{$0.id == self.userId}.first
        }
    }
    dynamic var userId: String!
    var liveQuestions: [Question]! {
        didSet {
            if let questions = liveQuestions {
                self.questionIds.append(objectsIn: questions.map{$0.id})
            }
        }
    }
    var savedQuestions: [Question] {
        get {
            let realm = try! Realm()
            let questions: [Question] = realm.objects(Question.self).filter{$0.testId == self.id}
            return questions
        }
    }
    dynamic var questionIds = List<String>()
    var attemptedAt: String!
    
    convenience init(user: User, attemptedAt: String) {
        self.init()
        self.liveUser = user
        self.userId = user.id
        self.questionIds = List<String>()
        self.attemptedAt = attemptedAt
    }
    
    override class func primaryKey() -> String? {
        return Properties.id.rawValue
    }
    
    override class func ignoredProperties() -> [String] {
        return  [Properties.liveUser.rawValue,Properties.savedUser.rawValue, Properties.liveQuestions.rawValue,Properties.savedQuestions.rawValue,]
    }
    
}


@objcMembers class Question: Object {
    
    enum Properties: String {
        case id, question, answers, canSelectMultipleAnswer, selectedAnswers
    }
    
    dynamic var id: String = UUID().uuidString
    dynamic var testId: String!
    dynamic var question: String!
    var answers =  List<String>()
    dynamic var canSelectMultipleAnswer = false
    dynamic var isMultipleChoseEnable = false
    dynamic var selectedAnswers = List<String>()
    
    convenience init(testId: String, question: String, answers: [String], canSelectMultipleAnswer: Bool,isMultipleChoseEnable: Bool){
        print(answers)
        self.init()
        self.id = UUID().uuidString
        self.testId = testId
        self.question = question
        self.answers.append(objectsIn: answers)
        self.canSelectMultipleAnswer = canSelectMultipleAnswer
        self.isMultipleChoseEnable = isMultipleChoseEnable
    }
    
    override class func primaryKey() -> String? {
        return Properties.id.rawValue
    }
}







